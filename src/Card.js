import React from 'react';
import './App.css';

const Card = ({name, phone, avatar, children, onChangeName, onDelete}) => {    
    return (
        <div className="card">
            <div className="container">
                <h4><b>{name}</b></h4> 
                <p>{phone}</p> 
                <p><input type="text" value={name} onChange={onChangeName} /></p> 
                <p><button className="button button3" onClick={onDelete}>Delete</button></p> 
                <div>{children}</div> 
            </div>
        </div>
    )
}

export default Card
