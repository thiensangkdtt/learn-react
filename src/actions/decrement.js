const decrement = (value) => {
    return {
        type: 'DECREMENT',
        payload: value
    }
}

export default decrement;