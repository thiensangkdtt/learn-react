import styled, { css } from 'styled-components';

const Button = styled.button`
  border: none;
  ${
   (props) => props.color && css`
    background-color: ${props => props.cards.length>2? props.theme[props.color] : props.cards.length<2? "#f44336" : "pink"};
    color: ${props => props.cards.length<=1? "black":"white"};
   `
  }
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
`

export default Button;