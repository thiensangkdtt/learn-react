// import {banana as BANA, apple} from './component1';
// import * as fruit from './component1';
// import {canada, vietnam} from './component2';
import React from 'react';
import './App.css';
import {useSelector, useDispatch} from 'react-redux'
import increment from './actions/increment'
import decrement from './actions/decrement'

function App() {  
  const count = useSelector(state => state.counterReducer)
  const logged = useSelector(state => state.loggedReducer)
  const dispatch = useDispatch()

  return (
      <div className="App">
          Count: {count}
          <p></p>
          <button onClick={() => {dispatch(increment(5))}}>+</button>
          <button onClick={() => {dispatch(decrement(10))}}>-</button>
          <p></p>
          {logged ? <h3>You are logged in the app</h3> : <h3>Please sign in now</h3>}
      </div>
  );
}

export default App;
