import React, { useContext } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, ButtonGroup, Badge } from 'reactstrap';
import { CounterContext } from '../App'

const ComponentC = () => {
    const counterContext = useContext(CounterContext)
    const { counter, dispath } = counterContext
    return (
        <div>
            <div>ComponentA</div>
            <ButtonGroup>
                <div>
                    <Button color="primary">
                        Counter1 <Badge color="secondary">{counter}</Badge>
                    </Button>
                </div>
            </ButtonGroup>
            <p></p>
            <ButtonGroup>
                <Button color="dark" onClick={() => dispath({type:"Increment", payload:5})}>Increment</Button>
                <Button color="warning" onClick={() => dispath({type:"Decrement", payload:5})}>Decrement</Button>
                <Button color="danger" onClick={() => dispath({type:"Reset"})}>Reset</Button>
            </ButtonGroup>
        </div>
    )
}

export default ComponentC
