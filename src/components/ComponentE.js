import React, { useEffect } from 'react'

let renderCount = 1
const ComponentE = (props) => {
    useEffect(() => {
       renderCount++;
    })

    return (
        <div>
            <h1>
                ComponentE: render {renderCount} || Counter: {props.count}
            </h1>
        </div>
    )
}

export default ComponentE
