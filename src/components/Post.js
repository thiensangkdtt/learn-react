import React, {useEffect, useState} from 'react'
import axios from 'axios'

const Post = (props) => {
    console.log(props)
    const id = props.match.params.post_id
    const [post, setPost] = useState([])

    useEffect(() => {
       (async() => {
            const res = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
            setPost(res.data)
       })()
    }, [])

    return (
        <div className="card shadow" key={id}>
            <h5>{id}</h5>
            <div className="card-body">
                <h5 className="card-title">{post.title}</h5>
                <p className="card-text">{post.body}</p>
            </div>
        </div>
    )
}

export default Post
