import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, ButtonGroup, Badge } from 'reactstrap';
import useCounter from '../customhook/useCounter'

const ComponentK = () => {
    const [count, increment, reset, decrement] = useCounter(0, 5);
    return (
        <div>
            <div>ComponentK</div>
            <ButtonGroup>
                <div>
                    <Button color="primary">
                        CounterK Counter <Badge color="secondary">{count}</Badge>
                    </Button>
                </div>
            </ButtonGroup>
            <p></p>
            <ButtonGroup>
                <Button color="dark" onClick={increment}>Increment</Button>
                <Button color="warning" onClick={decrement}>Decrement</Button>
                <Button color="danger" onClick={reset}>Reset</Button>
            </ButtonGroup>
        </div>
    )
}

export default ComponentK;
