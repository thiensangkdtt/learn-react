import React, {useEffect} from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import auth from '../auth'

const Navbar = (props) => {
    const handleAuth = () => {
        if(auth.isAuthenticated()) {
            auth.logout(() => {
                console.log(auth.authenticated)
                props.history.push("/")
            })
        } else {
            auth.login(() => {
                console.log(auth.authenticated)
                props.history.push("/about")
            })
        }
    }

    const authText = auth.isAuthenticated() ? 'Logout': 'Login'

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav mr-auto">
                    <li className="nav nav-tabs">
                        <NavLink className="nav-link" to="/">Home<span className="sr-only">(current)</span></NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/about">About</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/contact">Contact</NavLink>
                    </li>
                    <button type="button" className="btn btn-success" onClick={handleAuth}>{authText}</button>
                </ul>
            </div>
        </nav>
    )
}

export default withRouter(Navbar)
