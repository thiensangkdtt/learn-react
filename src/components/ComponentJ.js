import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, ButtonGroup, Badge } from 'reactstrap';
import useCounter from '../customhook/useCounter'

const ComponentJ = () => {
    const [count, increment, reset, decrement] = useCounter(10);
    return (
        <div>
            <div>ComponentJ</div>
            <ButtonGroup>
                <div>
                    <Button color="primary">
                        CounterJ Counter <Badge color="secondary">{count}</Badge>
                    </Button>
                </div>
            </ButtonGroup>
            <p></p>
            <ButtonGroup>
                <Button color="dark" onClick={increment}>Increment</Button>
                <Button color="warning" onClick={decrement}>Decrement</Button>
                <Button color="danger" onClick={reset}>Reset</Button>
            </ButtonGroup>
        </div>
    )
}

export default ComponentJ;
