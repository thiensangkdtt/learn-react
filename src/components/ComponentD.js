import React, { useEffect } from 'react'

let renderCount = 1
const ComponentD = (props) => {
    useEffect(() => {
       renderCount++;
    })

    return (
        <div>
            <h1>
                ComponentD: render {renderCount} || Counter: {props.count}
            </h1>
        </div>
    )
}

export default ComponentD
