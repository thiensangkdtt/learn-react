import counterReducer from "./counterReducer"
import loggedReducer from "./loggedReducer"
import {combineReducers} from "redux"

const finalReducer = combineReducers({
    counterReducer, 
    loggedReducer, 
})

export default finalReducer;